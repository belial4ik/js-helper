// Array.concat(arr1?, arr2?, … )

//Создает новый массив, который объединяет все переданные в метод массивы. Если один из переданных
// аргументов не является массивом, то он добавится в результирующий массив, как его элемент

const array1 = ['a','b','c'];
const array2 = ['d','e'];
const arrayItem = 'f';

const res = array1.concat(array2, arrayItem);
console.log(res);  //[ 'a', 'b', 'c', 'd', 'e', 'f' ]