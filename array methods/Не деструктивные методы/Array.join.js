//Array.join(separator?)

//Метод конвертирует и возвращает строку, в которой каждый элемент массива будет разделен символом,
//переданным в качестве separator. Метод лучше всего работает с элементами массива типов string и number.
//Если separator не передан, по умолчанию будет использоваться « , ».

const array = [ 'a', 'b', 'c', 'd', 'e', 'f' ];
const res = array.join('-');
console.log(res);  //a-b-c-d-e-f