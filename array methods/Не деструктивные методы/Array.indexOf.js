//Array.indexOf(searchValue, startIndex?)

// Метод проверяет имеется ли в массиве searchValue, начиная с позиции startIndex. Возвращает индекс первого вхождения,
// а если элемент не найден — возвращает -1. Если startIndex отрицательный, то он отсчитывается,
// как смещение с конца массива, а также, если startIndex не указывается — поиск идет по всему массиву.
// Не работает для NaN.

const arr = [1,2,3,4,5,6,7];

const trueInd = arr.indexOf(4)
console.log(trueInd);  //3
const falseInd = arr.indexOf(20)
console.log(falseInd);  //-1