// Array.slice(startIndex?, endIndex?)

// Метод копирует элементы в новый массив, начиная с позиции startIndex до endIndex,
// не включая его, то есть с startIndex до endIndex-1. Если любой из индексов отрицателен,
// то к нему добавляется длина массива, таким образом, (-1) относится к последнему элементу и так далее.

const array = ['a','b','c','d','e'];

const sliceIndex = array.slice(1,3)
const sliceNegativeIndex = array.slice(1,-1)

console.log(sliceIndex);
//["b", "c"]
console.log(sliceNegativeIndex);
//["b", "c", "d"]