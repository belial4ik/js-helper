//Array.includes()

//Метод проверяет существует ли элемент в массиве. Возвращает true или false.

const array = [1,2,3,4,5];

const includArray = array.includes(2)
console.log(includArray); //true
const includArray2 = array.includes(6)
console.log(includArray2);  //false