//Array.reduce(callback, initialValue?): Возвращает одно результирующее значение

// Приводит все элементы к единому значению, и функция reducer принимает значение и аккумулятор.
// Аккумулятор должен быть установлен на начальное значение;
// метод возвращает измененную копию массива, не преобразуя оригинальный массив.

const numbers = [5,42,56,3,45]
const res = numbers.reduce((previousValue,currentValue) => previousValue + currentValue)
console.log(res);    //151