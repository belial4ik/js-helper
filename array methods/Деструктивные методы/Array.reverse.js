//Array.reverse()

// Переворачивает порядок следования элементов массива, и возвращает измененный исходный массив.

const array = ['a','b','c','d','e']
const newArray = array.reverse();
console.log(array);  //[ 'e', 'd', 'c', 'b', 'a' ]
console.log(newArray);  //[ 'e', 'd', 'c', 'b', 'a' ]