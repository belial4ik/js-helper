//Array.sort(compareFunction?)

// Сортирует массив, и возвращает его. Данный метод сортирует значения, конвертируя их в строки,
// а значит числа не будут отсортированы. Для сортировки числовых элементов, в метод нужно
// передать compareFunction — функцию, определяющую порядок сортировки.

const countries = ['ukraine','russia','brazil','argentina','france','zanzibar','germany','england']
const sortedCountries = countries.sort();
console.log(sortedCountries);  //["argentina", "brazil", "england", "france", "germany", "russia", "ukraine", "zanzibar"]

const numbers = [4,-4,0,1,15,-1,32,9]
// const firstSorted = numbers.sort((a,b) => a - b)
const secondSorted = numbers.sort((a,b) => b - a)
// console.log(firstSorted);  //[-4, -1, 0, 1, 4, 9, 15, 32]
console.log(secondSorted);  //[32, 15, 9, 4, 1, 0, -1, -4]