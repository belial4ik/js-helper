//Array.fill(newElement, startIndex, endIndex)

// Заменяет элементы от startIndex до endIndex на newElement.
// Если startIndex не передан — заполнение начнется с 0 индекса,
// если endIndex не указан — элементы заменятся до конца массива

const array = [1,2,3,4,5,6,7,8,9];
const filledArray = array.fill('hope',1,5);
console.log(filledArray);  //[1, "hope", "hope", "hope", "hope", 6, 7, 8, 9]