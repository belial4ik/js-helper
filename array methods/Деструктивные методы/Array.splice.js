//Array.splice(startIndex, deleteCount?, elem1?, elem2?, …)

// Начиная со стартового индекса, удаляет количество элементов равное deleteCount,
// и вставляет переданные элементы. Проще говоря, начиная с позиции startIndex,
// количество элементов равное deleteCount заменяется на elem1, elem2, и т.д.
// Метод возвращает удаленные элементы. Если передать отрицательный startIndex, отсчет будет с конца массива.

const array = ['a','b','c','d','e']
const newArray = array.splice(1,3,'hello')
console.log(array); //[ 'a', 'hello', 'e' ]
console.log(newArray);  //[ 'b', 'c', 'd' ]