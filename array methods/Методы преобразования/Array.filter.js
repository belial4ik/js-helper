//Array.filter(callback, thisValue?): Оставляет лишь некоторые элементы

// Оставляет лишь те элементы, которые удовлетворяют условию, заданному в callback функции,
// и возвращает измененную копию массива, не преобразуя оригинальный массив.

const numbers = [1,2,3,4,5];
const res = numbers.filter(value => value > 3);
console.log(res);    //[ 4, 5 ]