// Array.every(callback, thisValue?)

// Возвращает true, если callback функция вернет true для каждого элемента.
// Обход по массиву прервется, как только callback вернет false

const array = [12, 5, 8, 130, 44];
const result = array.every(element => element >= 10)

const array2 = [12, 54, 18, 130, 44];
const result2 = array2.every(element => element >= 10)

console.log(result);  //false
console.log(result2);  //true