//Array.some(callback, thisValue?)

// Возвращает true, если callback функция вернет true хотя бы для
// одного элемента массива. Обход по массиву прервется, как только
// c
//
// callback вернет true.

//Следующий пример проверяет наличие в массиве элемента, который больше 10.
const array = [2, 5, 8, 1, 4];
const res = array.some(element => element > 10)
console.log(res);   //false

const array2 = [12, 5, 8, 1, 4];
const res2 = array2.some(element => element > 10)
console.log(res2);  //true