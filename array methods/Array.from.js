const fruitBasket = ['apple', 'banana','orange','mango','orange','mango'];

const removeDuplicates = Array.from(new Set(fruitBasket));
console.log(fruitBasket);           //[ 'apple', 'banana', 'orange', 'mango', 'orange', 'mango' ]
console.log(removeDuplicates);      //[ 'apple', 'banana', 'orange', 'mango']