// Метод Object.freeze() предотвращает модификацию свойств и значений объекта и
// добавление или удаление свойств объекта.


// Initialize an object
const user = {
    username: 'belyal4ik',
    password: 'kiLLer123'
};

// Freeze the object
const newUser = Object.freeze(user)
// newUser.password = '********'; - не сработает,так как объект защищен от перезаписи
// newUser.active = true; - не сработает,так как объект защищен от перезаписи

console.log(newUser); //{ username: 'belyal4ik', password: 'kiLLer123' }

console.log(Object.isFrozen(newUser))