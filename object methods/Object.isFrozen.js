// Метод Object.isFrozen() позволяет определить, был ли объект заморожен или нет,
// и возвращает логическое значение

const user = {
    username: 'belyal4ik',
    password: 'kiLLer123'
};
console.log(Object.isFrozen(user)); //false

const newUser = Object.freeze(user)
console.log(Object.isFrozen(newUser)); //true