const name = { firstName: 'Belial', lastName: 'Selimov' };
const details = { job: 'Delivery Boy', employer: 'Planet Express' };
const assign = Object.assign(name,details);
console.log(assign);
//{ firstName: 'Belial',lastName: 'Selimov',job: 'Delivery Boy',employer: 'Planet Express'}

//----------------------------------------------------------------------------------------------

// ALTERNATIVE

const copyObj = {...name,...details}
console.log(copyObj)
//{ firstName: 'Belial',lastName: 'Selimov',job: 'Delivery Boy',employer: 'Planet Express'}