// Метод Object.getPrototypeOf() используется для получения внутреннего
// скрытого [[Prototype]] объекта, также доступного через свойство __proto__.

const employees = ['Ron', 'April', 'Andy', 'Leslie'];
console.log(Object.getPrototypeOf(employees)); //[constructor: ƒ, concat: ƒ, copyWithin: ƒ, fill: ƒ, find: ƒ, …]

console.log(Object.getPrototypeOf(employees) === Array.prototype) //true