const employees = {
   boss: 'Michael',
   secretary: 'Pam',
   sales: 'Jim',
   accountant: 'Oscar'
};

const values = Object.values(employees);
console.log(values);
//["Michael", "Pam", "Jim", "Oscar"]