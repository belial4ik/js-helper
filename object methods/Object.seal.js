// Метод Object.seal()предотвращает добавление новых свойств объекта, но позволяет изменять
// существующие свойства. Этот метод похож на Object.freeze(). Обновите консоль,
// прежде чем выполнить нижеприведенный код, чтобы избежать ошибки.

const user = {
    username: 'belyal4ik',
    password: 'kiLLer123'
};

const newUser = Object.seal(user)
newUser.password = '*****'; //password был изменен
newUser.active = true; // свойство active: true не будет добавлено
console.log(newUser); //{ username: 'belyal4ik', password: '*****' }