const employees = {
   boss: 'Michael',
   secretary: 'Pam',
   sales: 'Jim',
   accountant: 'Oscar'
};

const keys = Object.keys(employees);
console.log(keys);
// ["boss", "secretary", "sales", "accountant"]